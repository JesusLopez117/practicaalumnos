import {pool} from "../db.js";
let query = "SELECT * FROM alumnos";
let form = {};

//Función para mostrar a todos los alumnos
export const renderAlumnos = async (req, res) =>{
    try{
        form.counter -= 1;

        if(form.counter === 0) {
            form = {};
            query = "SELECT * FROM alumnos";
        }

        const [rows] = await pool.query(query);
        res.render("index.html", {
            alumnos: rows,
            form
        });


    }catch(error){
        console.log("Aqui sucede el error  " + error);
    }
}

//Función para ingresar un nuevo Alumno
export const createAlumno = async (req, res) =>{
    try{
        const newAlumno = req.body;

        //Inserción de los datos
        const rows = await pool.query("INSERT INTO alumnos set ?", [newAlumno]);
        console.log("Llego despues de la query");
        return res.status(200).send("Se insertaron con exito los datos");

    }catch(error){
        console.log("Sucedio un error a la hora de crear " + error);
        return res.status(400).send("Sucedio un error al Insertar");
    }
}

//Función para Eliminar Alumnos
export const deleteAlumno = async (req, res) =>{
    try{
        const { id } = req.params;
        const newAlumno = req.body;

        const result = await pool.query("DELETE FROM alumnos WHERE id = ?", [id]);
        return res.status(200).send("Alumno eliminado");
    }catch(error){
        return res.status(400).send("No se puedo eliminar");
    }
}

//Función para filtrar
function filterSearchAlumno(obj) {
	const result = {};
	// Expresión regular que extrae el contenido entre corchetes
	const regex = /\[(.*?)\]/;
	for (const key in obj) {
		if (key.startsWith('busquedaAlu')) {
			const match = key.match(regex); // buscamos el contenido entre corchetes
			if (match) {
				result[match[1]] = obj[key]; // agregamos la propiedad al objeto resultado
			}
		}
	}
	return result;
}


//Función para buscar un alumno
export const searchAlumno = async (req, res) => {
    try{
        let body = req.body;
        let busquedaAlu = filterSearchAlumno(body);

        if (Object.keys(busquedaAlu).length === 0) {
			return res.status(400).send("Añade contenido a la consulta");
		}

        query = `SELECT * FROM alumnos WHERE matricula = ${busquedaAlu.matricula}`;
        
        form.counter = 2;
        return res.status(200).send("Query creado exitosamente")
    }catch(error){
        console.log(error);
    }
}

//Función para editar un alumno
export const editAlumno = async (req, res) => {
    try{
        const { id } = req.params;
        const [result] = await pool.query("SELECT * FROM alumnos WHERE id = ?", [id]);

        if(result.length === 0){
            return res.redirect("/");
        }

        res.render("editAlumnos.html",{
            alumno: result[0]
        });

    }catch(error){
        console.log(error);
    }
}

//Función para actualizar los datos
export const updateAlumno = async (req, res) => {
    try{
        const { id } = req.params;

        const newAlumno = {
            id: id,
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        }

        await pool.query("UPDATE alumnos set ? WHERE id = ?", [newAlumno, id]);
        return res.redirect("/");

    }catch(error){
        console.log(error);
		return res.status(400).send("Sucedio un error");
    }
}