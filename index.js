import app from "./app.js";
import { port } from "./config.js";

app.listen(port);
console.log(`Servidor encendido en el puerto ${port}`);