"use strict";
//Declarar constantes
const form = document.querySelector("#form");
const btnActualizar = document.querySelector("#btnActualizar");

//Creación de funciones necesarias

//Función de mensaje de alerta
function showAlert(mensaje, titulo){
    const modalToggle = document.getElementById("alertModal");
    const miModal = new bootstrap.Modal("#alertModal", {keyboard: false});
    document.getElementById("alertTitle").innerHTML = titulo;
    document.getElementById("alertMessage").innerHTML = mensaje;
    miModal.show(modalToggle);
}

//Obtención de los datos de los inputs
const getInputs = () =>{
    return {
        id: parseInt(form['id'].value.trim()),
        matricula: form['matricula'].value.trim().substring(0,45),
        nombre: form['nombre'].value.trim().substring(0,45),
        domicilio: form['domicilio'].value.trim().substring(0,45),
        sexo: form['sexo'].value.trim().substring(0,1),
        especialidad: form['especialidad'].value.trim().substring(0,45)
    };
};

//Función para actualizar
async function updateAlumno(event){
    try{
        event.preventDefault();

        //Se obtinene los datos
        let { id, matricula, nombre, domicilio, sexo, especialidad } = getInputs();

        //Se validan campos vacios
        if(!id || !matricula || !nombre || !domicilio || !sexo || !especialidad) return showAlert("Existen campos vacios!! :c", "Error");
        
        //Creación de objeto a mandar peticion
        let alumnoModificado = {
            id,
            matricula,
            nombre,
            domicilio,
            sexo,
            especialidad
        }

        await axios.post(window.location.pathname, alumnoModificado,{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        showAlert("Se actualizó el registro con exito", "Resultado");

		setTimeout(() => {
			window.location.href = '/';
		}, 2000);

    
    }catch(error){
        showAlert(error.response.data, "Error");
    }
}

//Botones
btnActualizar.addEventListener("click", updateAlumno);
form.addEventListener("reset", (event) =>{
    event.preventDefault();

	window.location.pathname = window.location.pathname;

})