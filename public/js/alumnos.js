"use strict";

//Declaración de los elementos DOM
const form = document.querySelector("#form");
const btnAgregar = document.querySelector("#btnAgregar");
const btnConsultar = document.querySelector("#btnConsultar");

//CREACIÓN DE FUNCIONES
//Función para mostrar mensajes al usuario
function showAlert(mensaje, titulo){
    const modalToggle = document.getElementById("alertModal");
    const miModal = new bootstrap.Modal("#alertModal", {keyboard: false});
    document.getElementById("alertTitle").innerHTML = titulo;
    document.getElementById("alertMessage").innerHTML = mensaje;
    miModal.show(modalToggle);
}

//Obtención de los datos de los inputs
const getInputs = () =>{
    return {
        matricula: form['matricula'].value.trim().substring(0,45),
        nombre: form['nombre'].value.trim().substring(0,45),
        domicilio: form['domicilio'].value.trim().substring(0,45),
        sexo: form['sexo'].value.trim().substring(0,1),
        especialidad: form['especialidad'].value.trim().substring(0,45)
    };
};


//Función para ingresar un nuevo Alumno
async function insertAlumno(event){
    try{
        event.preventDefault();

        //Se obtinene los datos
        let { matricula, nombre, domicilio, sexo, especialidad } = getInputs();

        //Se validan campos vacios
        if(!matricula || !nombre || !domicilio || !sexo || !especialidad) return showAlert("Existen campos vacios!! :c", "Error");

        await axios.post('/add', {
            matricula,
            nombre,
            domicilio,
            sexo,
            especialidad
        },{
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        showAlert("Datos insertados con exito :D", "Resultado");

        setTimeout(() =>{
            window.location.href = "/";
        }, 2000);

    }catch(error){
        //Cacha algun error inesperado.
        console.log(error);
        showAlert(error.response.data, "Error");
    }
}

//Función para conseguir los datos para buscar un alumno
async function lookUpAlumnos(event) {
    try{
        event.preventDefault();

        //Condigo los datos
        let alumno = getInputs();

        let busquedaAlu = {};

        //Verificacion de los datos
        if(alumno.matricula) {
            busquedaAlu.matricula = alumno.matricula;
        }

        await axios.post('/', {busquedaAlu}, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        });

        window.location.pathname = window.location.pathname;

    }catch(error){
        showAlert(error.response.data, "Error");
    }
}

//Función para eliminar
async function eliminarAlumno(id){
    try{
        await axios.get(`/delete/${id}`);
        showAlert("Alumno elimindo con exito", "Resultado");

        setTimeout(() =>{
            window.location.reload();
        }, 2000);
    }catch(error){
        showAlert(error.response.data, "Error");
    }
}

//Funcionamiento de botones
form.addEventListener("submit", insertAlumno);
btnConsultar.addEventListener("click", lookUpAlumnos);
form.addEventListener("reset", (event) =>{
    event.preventDefault();

	window.location.pathname = window.location.pathname;

})