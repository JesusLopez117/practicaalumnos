//Constantes necesarias
import express from "express";
import path from "path";
import morgan from "morgan";
import ejs from "ejs";

//Importación de las rutas
import alumnosRutas from "./router/alumnosRutas.js";
import { fileURLToPath } from "url";

const app = express();
const __dirname = path.dirname(fileURLToPath(import.meta.url));

//Configuraciones
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//Cambiar extensiones ejs a html
app.engine("html", ejs.renderFile);

//Middlewares
app.use(morgan("short"));
app.use(express.urlencoded({extended: false}));

//Rutas
app.use(alumnosRutas);
import adminAlumnos from "./router/alumnosRutas.js";

const apiPaths = {
    adminAlumnos: "/views/index.html"
}

//Archivos estaticos
app.use(express.static(path.join(__dirname, "public")));
app.use(apiPaths.adminAlumnos, adminAlumnos);

//Iniciando el servidor
export default app;