//constantes necesarias
import { Router } from "express";
import {
    renderAlumnos,
    createAlumno,
    deleteAlumno,
    searchAlumno,
    editAlumno,
    updateAlumno
} from "../controller/alumnoControllers.js";
const router = Router();

router.get("/", renderAlumnos);
router.post("/", searchAlumno);
router.post("/add", createAlumno);
router.get("/delete/:id", deleteAlumno);
router.get("/update/:id", editAlumno);
router.post("/update/:id", updateAlumno);
export default router;